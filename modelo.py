#camada de modelagem do banco
from config import *

class Gabarito(db.Model):
    id = db.Column(db.Integer, primary_key=True) #id do gabarito
    id_prova_gabarito = db.Column(db.Integer)      #numero da prova ex 1-2-3-4 para ser referencia e cruzar com as que o aluno fez
    resposta_gabarito = db.Column(db.String(254)) #questao coreta e a questao ex a ou b ou c string ficou de 2 caracteres porque 1 estava dando erro

    def __str__(self):
        return str(self.id) + "," + str(self.id_prova_gabarito) + ", " + self.resposta_gabarito

    #json de entrada que insere no banco
    def json(self):
        return {
            "id" : self.id,
            "id_prova_gabarito" : self.id_prova_gabarito,
            "resposta_gabarito" : self.resposta_gabarito
        }

class ProvaAluno(db.Model):
    id = db.Column(db.Integer, primary_key=True)  #esse id esta aqui por obrigacao do db
    id_aluno = db.Column(db.String(5))            #numero de identificacao do aluno 
    id_prova = db.Column(db.Integer)              #essa resposta_prova significa qual prova o aluno esta fazendo 1-2-3 vai cruzar com a prova la em cima
    respostas = db.Column(db.String(254))       
    pesos = db.Column(db.String(254)) 


    def __str__(self):
        return str(self.id) + "," + self.id_aluno + "," + str(self.id_prova) + ", " + \
        ", " + self.respostas + "," + self.pesos

    def json(self):
        return {
            "id" : self.id,
            "id_aluno" : self.id_aluno,
            "id_prova" : self.id_prova,
            "respostas" : self.respostas,
            "pesos" : self.pesos
        }

if __name__ == "__main__":
    db.create_all()


#incersao de gabarito para teste
novo = Gabarito(id_prova_gabarito = "1", resposta_gabarito = "a")
novo2 = Gabarito(id_prova_gabarito = "1", resposta_gabarito = "b")
db.session.add(novo)
db.session.add(novo2)
db.session.commit()
gabaritos = db.session.query(Gabarito).all()

for e in gabaritos:
    print(e)
    print(e.json())

#incercao de uma prova para teste
nova = ProvaAluno(id_aluno = "089", id_prova = "1", respostas= "a", pesos = "2")
nova2 = ProvaAluno (id_aluno = "089", id_prova = "1", respostas= "c", pesos = "1")
db.session.add(nova)
db.session.add(nova2)
db.session.commit()
provas = db.session.query(ProvaAluno).all()

for p in provas:
    print(p)
    print(p.json())