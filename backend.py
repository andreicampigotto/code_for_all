#camada de backend, onde estao as APIs
from flask.globals import request
from config import *
from modelo import Gabarito, ProvaAluno

# Aqui estao as rotas para conectar com o frontend

@app.route("/")
def index():
    return "PAI TA ON"

@app.route("/listar_gabaritos", methods=['get'])
def listar_gabaritos():
    gabaritos = db.session.query(Gabarito).all()
    retorno = []
    
    for g in gabaritos:
        retorno.append(g.json())
    resposta = jsonify(retorno)
    resposta.headers.add("Access-Control-Allow-Origin", "*")
    
    return resposta

@app.route("/listar_provas", methods=['get'])
def listar_provas():
    provas= db.session.query(ProvaAluno).all()
    retorno = []
    
    for p in provas:
        retorno.append(p.json())
    resposta = jsonify(retorno)
    resposta.headers.add("Access-Control-Allow-Origin", "*")
    
    return resposta

app.run(debug = True) #para testar 
