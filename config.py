#camada de configiracoes para nao precisar colocar em todasas outras 
from flask_sqlalchemy import SQLAlchemy
from flask import Flask, jsonify #nao esquecer de pip3 install flask
#from flask_cors import CORS #nao esquecer de pip3 install flask_cors pip3 install CORS # permitir back receber json do front
import os


app = Flask(__name__)
#CORS(app)           #resolve mapeamento

# sqlalchemy com sqlite
caminho = os.path.dirname(os.path.abspath(__file__))
arquivobd = os.path.join(caminho, "banco_provas.db")
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + arquivobd
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False

db = SQLAlchemy(app)